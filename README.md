# Hashiwokakero #

Since I took waaay to long about this and my time is split, I'll leave it at this.
The code of the actual solver can be found in the Scripts/Hashiwokakero folder. It has no references to unity assemblies and thus can be used elsewhere. Unity only serves as a presenter.

To play around with the solver: <https://mkoning.bitbucket.io/hashiwokakero/>
Use the examples_puzzles.txt for input samples