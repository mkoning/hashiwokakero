using UnityEngine;

public class SlotPresenter : MonoBehaviour
{
	[SerializeField] private Option _horizontal;
	[SerializeField] private Option _vertical;
	
	public SlotNode _node;
	private int _lastConnectionCount = -1;

	// This is just for a visual representation.
	// I do not feel the need to add extra events in the Hashi to deal with performance
	private void Update()
	{
		if (_node.Bridge != null)
		{
			if (_node.Bridge.IsInactive)
			{
				_vertical.Head.SetActive(false);
				_horizontal.Head.SetActive(false);
				return;
			}

			if (_node.Bridge.IsHorizontal)
			{
				_vertical.Head.SetActive(false);
				_horizontal.Head.SetActive(true);
				_horizontal.Double.SetActive(_node.Bridge.ConnectionCount == 2);
				_horizontal.Single.SetActive(_node.Bridge.ConnectionCount == 1);
			}
			else
			{
				_horizontal.Head.SetActive(false);
				_vertical.Head.SetActive(true);
				_vertical.Double.SetActive(_node.Bridge.ConnectionCount == 2);
				_vertical.Single.SetActive(_node.Bridge.ConnectionCount == 1);
			}
		}
		else
		{
			_vertical.Head.SetActive(false);
			_horizontal.Head.SetActive(false);
		}
	}

	public void Initialize(SlotNode slotNode)
	{
		_node = slotNode;
	}

	[System.Serializable]
	public class Option
	{
		public GameObject Head;
		public GameObject Single;
		public GameObject Double;
	}
}