using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TrimText : MonoBehaviour
{
	private TMP_InputField _input;

	void Awake()
	{
		_input = GetComponent<TMP_InputField>();
	}

	public void Trim()
	{
		_input.text = _input.text.Trim('\r', '\n');
	}
}
