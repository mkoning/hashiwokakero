using System;
using System.Collections.Generic;

public class SlotNode : INode
{
	public Bridge Bridge { get; private set; }
	public event Action<Bridge> OnBridgeAdded;

	private Dictionary<int, SaveData> _saveData;

	public SlotNode()
	{
		_saveData = new Dictionary<int, SaveData>();
	}

	public void AddBridge(Bridge bridge)
	{
		Bridge = bridge;
		OnBridgeAdded?.Invoke(bridge);
		
	}

	public void Save(int iteration)
	{
		_saveData[iteration] = new SaveData
		            {
						Bridge = this.Bridge
		            };
	}

	public void Reset(int iteration)
	{
		Bridge = _saveData[iteration].Bridge;
	}

	private class SaveData
	{
		public Bridge Bridge;
	}
}

public interface INode
{
	void Save(int iteration);
	void Reset(int iteration);
}