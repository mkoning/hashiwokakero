using System;
using System.Collections.Generic;
using System.Linq;

public class IslandNode : INode
{
	private Dictionary<int, SaveData> _saveData;
	public int MaxPointCount { get; }
	public int AvailablePoints { get; private set; }
	public float Entrophy => (float)PossibleConnectionsCount / AvailablePoints;
	public bool IsComplete => AvailablePoints == 0;
	public int PossibleConnectionsCount => Bridges.Where(b => b.IsAvailable)
	                                              .Sum(b => 2 - b.ConnectionCount);
	public List<Bridge> Bridges { get; }

	public event Action OnUpdated;

	public IslandNode(int pointCount)
	{
		MaxPointCount = pointCount;
		AvailablePoints = pointCount;
		Bridges = new List<Bridge>();
		_saveData = new Dictionary<int, SaveData>();
	}

	public void AddBridgeConnection(int count)
	{
		AvailablePoints -= count;
		OnUpdated?.Invoke();
	}

	public void Save(int iteration)
	{
		_saveData[iteration] = new SaveData
		            {
						AvailablePoints = AvailablePoints
		            };
	}

	public void Reset(int iteration)
	{
		AvailablePoints = _saveData[iteration].AvailablePoints;
		OnUpdated?.Invoke();
	}

	public class SaveData
	{
		public int AvailablePoints;
	}
}