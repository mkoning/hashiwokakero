using System.Collections.Generic;
using System.Linq;

public class Hashi
{
	public List<INode> Nodes { get; }
	public int Width { get; }
	public int Height { get; }
	public string Raw { get; }

	public List<Bridge> Bridges { get; }

	public Hashi(string input)
	{
		input = input.Trim('\r', '\n');
		Raw = input;
		var output = input.Split('x', 'S');

		Width = int.Parse(output[0]);
		Height = int.Parse(output[1]);
		var data = output[2];

		Nodes = new List<INode>(data.Length);

		for (var i = 0; i < data.Length; i++)
		{
			var v = int.Parse(data[i].ToString());

			if (v == 0)
			{
				Nodes.Add(new SlotNode());
			}
			else
			{
				Nodes.Add(new IslandNode(v));
			}
		}

		Bridges = new List<Bridge>();
		AssignBridges();
	}
	private void AssignBridges()
	{
		for (var i = 0; i < Nodes.Count; i++)
		{
			// We only need the island nodes
			if (Nodes[i] is SlotNode)
			{
				continue;
			}

			// check for the closest neighbor
			CheckHorizontalConnection(i);
			CheckVerticalConnection(i);
		}
	}

	private void CheckHorizontalConnection(int i)
	{
		var island = (IslandNode) Nodes[i];
		var pointer = i;
		var left = i - (i % Width);
		while (pointer > left)
		{
			pointer--;
			if (HasNeighbourAt(pointer, island, true)) break;
		}
	}

	private void CheckVerticalConnection(int i)
	{
		var island = (IslandNode) Nodes[i];

		var pointer = i - Width;
		while (pointer >= 0)
		{
			if (HasNeighbourAt(pointer, island, false)) break;
			pointer -= Width;
		}
	}

	private bool HasNeighbourAt(int pointer, IslandNode island, bool isHorizontal)
	{
		if (Nodes[pointer] is IslandNode neighbour)
		{
			// check if this island already has a bridge with this neighbour
			if (island.Bridges.Any(b => b.Islands.Contains(neighbour)) == false)
			{
				//it does not, create a bridge
				var bridge = new Bridge(island, neighbour, isHorizontal);
				Bridges.Add(bridge);
				bridge.OnBridgeConnectionAdded += HandleBridgeConnectionAdded;
				island.Bridges.Add(bridge);
				neighbour.Bridges.Add(bridge);
			}

			return true;
		}

		return false;
	}

	private void HandleBridgeConnectionAdded(Bridge bridge)
	{
		var indexA = Nodes.IndexOf(bridge.Islands[0]);
		var indexB = Nodes.IndexOf(bridge.Islands[1]);

		if (bridge.IsHorizontal)
		{
			for (int i = indexB + 1; i < indexA; i++)
			{
				var slot = (SlotNode) Nodes[i];
				if (slot.Bridge == null)
				{
					slot.AddBridge(bridge);
				}
			}
		}
		else
		{
			for (int i = indexB + Width; i < indexA; i+=Width)
			{
				var slot = (SlotNode) Nodes[i];
				if (slot.Bridge == null)
				{
					slot.AddBridge(bridge);
				}
			}
		}
	}
}