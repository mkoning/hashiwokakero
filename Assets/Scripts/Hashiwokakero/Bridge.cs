using System;
using System.Collections.Generic;

public class Bridge
{
	public IslandNode[] Islands;
	public bool IsHorizontal { get; }
	public int ConnectionCount { get; private set; }
	public bool IsComplete { get; private set; }
	public bool IsAvailable => IsComplete == false && IsInactive == false;

	public event Action<Bridge> OnBridgeConnectionAdded;

	public bool IsInactive { get; private set; }
	private Dictionary<int, SaveData> _saveData;

	public Bridge(IslandNode a, IslandNode b, bool isHorizontal)
	{
		IsHorizontal = isHorizontal;
		Islands = new[] {a,b};
		ConnectionCount = 0;
		_saveData = new Dictionary<int, SaveData>();
	}

	public void AddConnection(int count)
	{
		ConnectionCount += count;
		
		foreach (var island in Islands)
		{
			island.AddBridgeConnection(count);
		}
		OnBridgeConnectionAdded?.Invoke(this);
	}

	public void DeActivate()
	{
		IsInactive = true;
	}

	public void MarkBridgeComplete()
	{
		IsComplete = true;
	}

	public void Save(int iteration)
	{
		_saveData[iteration] = new SaveData
		            {
						ConnectionCount = ConnectionCount,
						IsComplete = IsComplete,
						IsInactive = IsInactive
		            };
	}

	public void Reset(int iteration)
	{
		ConnectionCount = _saveData[iteration].ConnectionCount;
		IsComplete = _saveData[iteration].IsComplete;
		IsInactive = _saveData[iteration].IsInactive;
	}

	private class SaveData
	{
		public int ConnectionCount;
		public bool IsComplete;
		public bool IsInactive;
	}
}

