using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Solver
{
	private static int CurrentIteration;

	public static IEnumerator Solve(Hashi hashi, bool step = false)
	{
		if(step) yield return null;

		var allIslands = hashi.Nodes.OfType<IslandNode>().ToList();

		yield return Run(hashi, step, allIslands);

		if (IsSuccess(hashi))
		{
			Console.WriteLine("Solve Success");
			// Everything is resolved
			yield break;
		}

		CurrentIteration = -1;
		
		var openIslands = GetOpenIslands(hashi);
		yield return PickAndPropagate();

		if (IsSuccess(hashi))
		{
			Console.WriteLine("Solve Success");
			// Everything is resolved
			yield break;
		}

		// failed to solve
		Console.WriteLine("Failed to solve");
		
		IEnumerator PickAndPropagate()
		{
			yield return null;

			// If at the end of the runs the connection failed, try again from this point with another starting point.
			var bridges = openIslands.OrderBy(i => i.AvailablePoints)
			                         .SelectMany(i => i.Bridges)
			                         .Where(b => b.IsAvailable)
			                         .ToList();
			var count = bridges.Count;
			var index = 0;

			while (index < count)
			{
				// there are still islands to be resolved.
				// Save the current state
				CurrentIteration++;
				int current = CurrentIteration;
				SaveStates(hashi, current);

				// Choose a bridge to resolve and keep trying.
				AddBridgeConnection( bridges[index]);
				if(step) yield return null;
				yield return Run(hashi, step, allIslands);
				if(step) yield return null;
				
				if (IsSuccess(hashi))
				{
					break;
				}

				openIslands = GetOpenIslands(hashi);

				if (openIslands.Any())
				{
					yield return PickAndPropagate();
				}
				
				if (IsSuccess(hashi))
				{
					break;
				}
				if (openIslands.Any(i => i.Entrophy <= 0))
				{
					// this run has failed. try again
					openIslands = new List<IslandNode>();
				}
				
				index++;
				ResetStates(hashi, current);
			}
		}

		void AddBridgeConnection(Bridge bridge)
		{
			bridge.AddConnection(1);
			if (bridge.ConnectionCount == 2)
			{
				bridge.MarkBridgeComplete();
			}
			else if (bridge.Islands.Any(i => i.IsComplete))
			{
				bridge.MarkBridgeComplete();
			}
		}
	}

	private static bool IsSuccess(Hashi hashi)
	{
		return hashi.Nodes
		            .OfType<IslandNode>()
		            .All(i => i.IsComplete) 
		       && IsConnected(hashi);
	}

	private static void ResetStates(Hashi hashi, int iteration)
	{
		foreach (var node in hashi.Nodes)
		{
			node.Reset(iteration);
		}

		foreach (var bridge in hashi.Bridges)
		{
			bridge.Reset(iteration);
		}
	}

	private static void SaveStates(Hashi hashi, int iteration)
	{
		foreach (var node in hashi.Nodes)
		{
			node.Save(iteration);
		}

		foreach (var bridge in hashi.Bridges)
		{
			bridge.Save(iteration);
		}
	}

	private static IEnumerator Run(Hashi hashi, bool step, IEnumerable<IslandNode> allIslands)
	{
		var openIslands = GetOpenIslands(allIslands);
		var twoTwoBridges = hashi.Bridges.Where(b => b.Islands[0].MaxPointCount == 2 && b.Islands[1].MaxPointCount == 2);

		// First remove 1 - 1 islands connections. These can never be made
		for (var i = hashi.Bridges.Count - 1; i >= 0; i--)
		{
			var bridge = hashi.Bridges[i];
			if (bridge.Islands.Sum(island => island.MaxPointCount) == 2)
			{
				bridge.DeActivate();
				if (step) yield return null;
			}
		}

		openIslands = GetOpenIslands(allIslands);
		if (step) yield return null;
		
		// Loop over all open islands and try to resolve them
		while (openIslands.Any())
		{
			var island = openIslands.FirstOrDefault(i => i.Entrophy <= 1 && i.IsComplete == false);
			if (island != null)
			{
				// Island has only 1 possible option left. Apply these options
				var bridges = island.Bridges.Where(b => b.IsAvailable);

				if (bridges.Any())
				{
					foreach (var bridge in bridges)
					{
						bridge.AddConnection(2 - bridge.ConnectionCount);
						bridge.MarkBridgeComplete();
					}

					openIslands = GetOpenIslands(allIslands);
					if (step) yield return null;
					continue;
				}
			}

			// Find island that has one bridge left
			island = openIslands.FirstOrDefault(i => i.AvailablePoints <= 2 && i.AvailablePoints > 0
			                                         && i.Bridges.Count(b => b.IsAvailable) == 1);
			if (island != null)
			{
				var bridge = island.Bridges.First(b => b.IsAvailable);
				bridge.AddConnection(island.AvailablePoints);
				bridge.MarkBridgeComplete();

				openIslands = GetOpenIslands(allIslands);
				if (step) yield return null;
				continue;
			}

			// Cleanup islands that are completed but still have bridge connections
			island = GetDirtyIsland();

			if (island != null)
			{
				var bridges = island.Bridges.Where(b => b.IsAvailable).ToArray();
				for (var i = bridges.Length - 1; i >= 0; i--)
				{
					var bridge = bridges[i];
					if (bridge.ConnectionCount == 0)
					{
						bridge.DeActivate();
					}
					else
					{
						bridge.MarkBridgeComplete();
					}
				}

				if (step) yield return null;
				continue;
			}

			// find and remove possible bridges that will cross existing bridges.
			var crossingBridges = GetCrossingBridges(hashi);

			if (crossingBridges.Any())
			{
				for (var i = crossingBridges.Length - 1; i >= 0; i--)
				{
					crossingBridges[i].DeActivate();
					if (step) yield return null;
				}

				openIslands = GetOpenIslands(allIslands);
				if (step) yield return null;
				continue;
			}

			// deal with 2 - 2 bridges.
			var dirtyBridges = twoTwoBridges.Where(b => b.IsAvailable && b.ConnectionCount == 1);
			if (dirtyBridges.Any())
			{
				foreach (var b in dirtyBridges)
				{
					b.MarkBridgeComplete();
				}

				openIslands = GetOpenIslands(allIslands);
				if (step) yield return null;
				continue;
			}

			island = GetAbsoluteIsland();
			if (island != null)
			{
				var bridges = island.Bridges.Where(b => b.ConnectionCount == 0 && b.IsAvailable);
				foreach (var bridge in bridges)
				{
					bridge.AddConnection(1);
					if (step) yield return null;
				}

				openIslands = GetOpenIslands(allIslands);
				if (step) yield return null;
				continue;
			}

			if (step) yield return null;
			break;
		}

		IslandNode GetDirtyIsland()
		{
			return allIslands.FirstOrDefault(i => i.IsComplete
			                                      && i.Bridges.Any(b => b.IsAvailable));
		}

		IslandNode GetAbsoluteIsland()
		{
			return openIslands.FirstOrDefault(i => i.AvailablePoints - i.PossibleConnectionsCount >= -1
			                                       && i.Bridges.Any(b => b.IsAvailable && b.ConnectionCount == 0));
		}
	}

	static IEnumerable<IslandNode> GetOpenIslands(IEnumerable<IslandNode> allIslands)
	{
		return allIslands.Where(i => i.AvailablePoints > 0);
	}

	static IEnumerable<IslandNode> GetOpenIslands(Hashi hashi)
	{
		return hashi.Nodes.OfType<IslandNode>().Where(i => i.IsComplete == false);
	}

	/// <summary>
	/// Iterate over the islands via their connections. Return if all islands are connected.
	/// Note that all islands should be completed. Check that before running this code.
	/// </summary>
	private static bool IsConnected(Hashi hashi)
	{
		var allIslands = hashi.Nodes
		                      .OfType<IslandNode>()
		                      .ToList();

		ClearIsland(allIslands.First());

		return allIslands.Count == 0;

		void ClearIsland(IslandNode island)
		{
			allIslands.Remove(island);
			foreach (var bridge in island.Bridges
			                             .Where(b => b.IsInactive == false && b.IsComplete))
			{
				var neighbour = bridge.Islands.FirstOrDefault(i => i != island);
				if (neighbour != null && allIslands.Contains(neighbour))
				{
					ClearIsland(neighbour);
				}
			}
		}
	}

	/// <summary>
	/// Returns bridges that cross other, already placed, bridges.
	/// </summary>
	private static Bridge[] GetCrossingBridges(Hashi hashi)
	{
		var bridges = new List<Bridge>();

		var openBridges = hashi.Bridges.Where(b => b.IsAvailable
		                                           && b.ConnectionCount == 0);

		foreach (var bridge in openBridges)
		{
			var nodes = hashi.Nodes;
			var indexA = nodes.IndexOf(bridge.Islands[0]);
			var indexB = nodes.IndexOf(bridge.Islands[1]);

			if (bridge.IsHorizontal)
			{
				for (int j = indexB + 1; j < indexA; j++)
				{
					var slot = (SlotNode) nodes[j];
					if (slot.Bridge != null)
					{
						bridges.Add(bridge);
					}
				}
			}
			else
			{
				for (int j = indexB + hashi.Width; j < indexA; j += hashi.Width)
				{
					var slot = (SlotNode) nodes[j];
					if (slot.Bridge != null)
					{
						bridges.Add(bridge);
					}
				}
			}
		}

		return bridges.ToArray();
	}
}
