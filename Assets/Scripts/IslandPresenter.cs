using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IslandPresenter : MonoBehaviour
{
	[SerializeField] private TMP_Text _label;
	[SerializeField] private Image _image;
	public IslandNode _node;
	public void Initialize(IslandNode node)
	{
		_node = node;
		_label.text = $"{node.MaxPointCount.ToString()}";

		_node.OnUpdated += HandleOnIslandUpdated;
	}

	void OnDestroy()
	{
		_node.OnUpdated -= HandleOnIslandUpdated;
	}

	void HandleOnIslandUpdated()
	{
		_image.color = _node.IsComplete ? Color.green : Color.white;
	}
}
