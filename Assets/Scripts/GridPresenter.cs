using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GridPresenter : MonoBehaviour
{
	[SerializeField] private HashiController _hashiController;
	[SerializeField] private IslandPresenter _islandPrefab;
	[SerializeField] private SlotPresenter _emptySlotPrefab;
	[SerializeField] private GridLayoutGroup _grid;

	IslandNode[] islands;

	void Awake()
	{
		_hashiController.OnPuzzleGenerated += HandlePuzzleGenerated;
	}

	public void HandlePuzzleGenerated()
	{
		ClearGrid();

		var rect = _grid.GetComponent<RectTransform>().rect.size;
		var smallest = Mathf.Min(rect.x / _hashiController.Width, rect.y / _hashiController.Height);
		_grid.cellSize = new Vector2(smallest, smallest);

		var isWidthSmaller = rect.x / _hashiController.Width <= rect.y / _hashiController.Height;
		if (isWidthSmaller)
		{
			_grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
			_grid.constraintCount = _hashiController.Width;
		}
		else
		{
			_grid.constraint = GridLayoutGroup.Constraint.FixedRowCount;
			_grid.constraintCount = _hashiController.Height;
		}

		var nodes = _hashiController.Nodes;
		for (var i = 0; i < _hashiController.Nodes.Count; i++)
		{
			if (nodes[i] is IslandNode)
			{
				var presenter = Instantiate(_islandPrefab, _grid.transform, false);
				presenter.Initialize((IslandNode) nodes[i]);
				presenter.gameObject.name = $"{i} Island";
			}
			else
			{
				var slot = Instantiate(_emptySlotPrefab, _grid.transform, false);
				slot.Initialize((SlotNode) nodes[i]);
				slot.gameObject.name= $"{i} ()";
			}
		}
	}

	private void ClearGrid()
	{
		for (int i = _grid.transform.childCount - 1; i >= 0; i--)
		{
			Destroy(_grid.transform.GetChild(i).gameObject);
		}
	}
}
