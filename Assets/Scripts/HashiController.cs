using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HashiController : MonoBehaviour
{
	[SerializeField] private string _input = "7x7S2000404000000050020000300400000000001000004000403";
	[SerializeField] private TMP_InputField _inputField;
	[SerializeField] private Button _solveButton;
	[SerializeField] bool _step;
	private Hashi _hashi;

	private int _width;
	private int _height;

	private INode[] _nodes;
	public event Action OnPuzzleGenerated;

	public List<INode> Nodes => _hashi.Nodes;
	public List<Bridge> Bridges => _hashi.Bridges;
	public int Width => _hashi.Width;
	public int Height => _hashi.Height;

	private void Start()
	{
		_inputField.text = _input;
		_solveButton.onClick.AddListener(() =>
		{
			SolvePuzzle(_inputField.text);
		});

		CreatePuzzle(_input);
	}

	public void CreatePuzzle(string input)
	{
		try
		{
			_hashi = new Hashi(input);
		}
		catch (Exception e)
		{
			Debug.LogError($"Failed generating Hashi with error: {e.Message}\n{e.StackTrace}");
			return;
		}

		OnPuzzleGenerated?.Invoke();
	}

	public void SolvePuzzle(string input)
	{
		if (_hashi == null || _hashi.Raw != input)
		{
			CreatePuzzle(input);
		}

		StartCoroutine(Solver.Solve(_hashi, _step));
	}
}

